const express = require("express");
const app = express();
const port = 3000;
app.use(express.static("public"));
app.use("/img", express.static("assets"));
app.set("view engine", "ejs");

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/game", (req, res) => {
  res.render("game");
});

app.get("/user", (req, res) => {
  const data = require("./data/user.json");
  console.log(data);
  res.send(data);
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
