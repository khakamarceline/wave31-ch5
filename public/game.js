const batuBtn = document.getElementById("batu");
const kertasBtn = document.getElementById("kertas");
const guntingBtn = document.getElementById("gunting");

const batuCom = document.getElementById("batuCom");
const kertasCom = document.getElementById("kertasCom");
const guntingCom = document.getElementById("guntingCom");

const pembungkusVs = document.getElementById("pembungkusVs");
const versus = document.getElementById("versus");
const playerWin = document.getElementById("playerWin");
const comWin = document.getElementById("comWin");
const draw = document.getElementById("draw");

const myReset = document.getElementById("resetTombol");

let pilihanUser = "";
let pilihanCom = "";

versus.style.display = "";
playerWin.style.display = "none";
comWin.style.display = "none";
draw.style.display = "none";

batuBtn.onclick = () => {
  batuBtn.style.backgroundColor = "#C4C4C4";
  batuBtn.style.borderRadius = "50px";
  pilihanUser = "batu";
  computerMemilih();
  disableClick();
};

kertasBtn.onclick = () => {
  kertasBtn.style.backgroundColor = "#C4C4C4";
  kertasBtn.style.borderRadius = "50px";
  pilihanUser = "kertas";
  computerMemilih();
  disableClick();
};

guntingBtn.onclick = () => {
  guntingBtn.style.backgroundColor = "#C4C4C4";
  guntingBtn.style.borderRadius = "50px";
  pilihanUser = "gunting";
  computerMemilih();
  disableClick();
};

const computerMemilih = () => {
  const pilihanYangTersedia = ["batuCom", "kertasCom", "guntingCom"];
  const pilihanRandom = Math.floor(Math.random() * 3);
  const pilihanKomputer = pilihanYangTersedia[pilihanRandom];
  pilihanCom = pilihanYangTersedia[pilihanRandom];
  backgroundCom(pilihanKomputer);
  hasilPermainan();
};

const backgroundCom = (pilihanKomputer) => {
  const selectedBox = document.getElementById(pilihanKomputer);
  selectedBox.style.backgroundColor = "#C4C4C4";
  selectedBox.style.borderRadius = "50px";
};

const disableClick = () => {
  batuBtn.style.pointerEvents = "none";
  kertasBtn.style.pointerEvents = "none";
  guntingBtn.style.pointerEvents = "none";
  batuCom.style.pointerEvents = "none";
  kertasCom.style.pointerEvents = "none";
  guntingCom.style.pointerEvents = "none";
};

const hasilPermainan = () => {
  if (pilihanUser === "batu" && pilihanCom === "batuCom") {
    console.log("draw");
    versus.style.display = "none";
    draw.style.display = "";
  } else if (pilihanUser === "batu" && pilihanCom === "kertasCom") {
    console.log("com win");
    versus.style.display = "none";
    comWin.style.display = "";
  } else if (pilihanUser === "batu" && pilihanCom === "guntingCom") {
    console.log("player 1 win");
    versus.style.display = "none";
    playerWin.style.display = "";

    // kertas
  } else if (pilihanUser === "kertas" && pilihanCom === "kertasCom") {
    console.log("draw");
    versus.style.display = "none";
    draw.style.display = "";
  } else if (pilihanUser === "kertas" && pilihanCom === "batuCom") {
    console.log("player 1 win");
    versus.style.display = "none";
    playerWin.style.display = "";
  } else if (pilihanUser === "kertas" && pilihanCom === "guntingCom") {
    console.log("com win");
    versus.style.display = "none";
    comWin.style.display = "";

    // gunting
  } else if (pilihanUser === "gunting" && pilihanCom === "guntingCom") {
    console.log("draw");
    versus.style.display = "none";
    draw.style.display = "";
  } else if (pilihanUser === "gunting" && pilihanCom === "batuCom") {
    console.log("comp win");
    versus.style.display = "none";
    comWin.style.display = "";
  } else if (pilihanUser === "gunting" && pilihanCom === "kertasCom") {
    console.log("player 1 win");
    versus.style.display = "none";
    playerWin.style.display = "";
  } else {
    console.log("error");
  }
};

myReset.onclick = () => {
  window.location.reload();
};
